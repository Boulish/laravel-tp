<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Firstcontroller@home');
Route::get('/home', 'Firstcontroller@home');

Route::get('/singleton', 'Firstcontroller@singleton');

Route::get('/facade', 'Firstcontroller@testFacade');

Route::get('/iterateur', 'Firstcontroller@testIterator');

Route::get('/observer', 'Firstcontroller@testObserver');

Route::get('/strategy', 'Firstcontroller@testStrategy');

Route::get('/template', 'Firstcontroller@testTemplate');

Route::get('/command', 'Firstcontroller@testCommand');

Route::get('/composite', 'Firstcontroller@testComposite');

Route::get('/chain_responsability', 'Firstcontroller@testResponsabilityChain');

Route::get('/state', 'Firstcontroller@testState');
