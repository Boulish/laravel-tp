<?php

namespace App;

class Subject
{
    private $obervers;

    public function __construct(){
        $this->observers = [];
    }

    public function addObserver($observer){
        array_push($this->observers, $observer);
    }

    public function removeObserver($obersver){
        if(in_array($obersver)){
            $key = array_search($oberver);
            array_splice($this->obervers, $key, 1);
        }
    }

    public function notifyObservers(){
        foreach ( $this->observers as $obs){
            $obs->update();
        }
    }
}
