<?php

namespace App;

use App\FactoryClass;
use App\Iterator;

class ArrayVoiture 
{
    private $voitures = null;
    
    public function __construct($voitures){
        $this->voitures=$voitures;
    }

    public function initIterator(){
        return new Iterator($this->voitures);
    }

}
