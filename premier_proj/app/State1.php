<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State1 extends Model
{
    public function __construct()
    {
        
    }

    public function operation($monInt)
    {
        return $monInt-1;
    }
}
