<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State0 extends Model
{
    public function __construct()
    {
        
    }

    public function operation($monInt)
    {
        return $monInt+1;
    }
}
