<?php 

namespace App;

use App\VoitureTemplate;

class HyundaiVoiture extends VoitureTemplate{

    public function getOptions(){
        return parent::getOptions() . ' - 9 places du hyundai satelite';
    }
}