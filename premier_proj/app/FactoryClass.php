<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FactoryClass
{

    public function __construct(){
    }

    public static function create_car(String $typeVoiture, $nom){

        if($typeVoiture === 'fiat'){
            return new FiatClass($nom);
        }
        elseif($typeVoiture === 'hyundai'){
            return new HyundaiClass($nom);
        }
        else{
            echo 'Type non reconnu';
            return false;
        }
    }

    public static function toString(){
        return 'Usine de voiture';
    }
}
