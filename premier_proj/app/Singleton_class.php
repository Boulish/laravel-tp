<?php

namespace App;

class Singleton_class
{
    private $counter;
    private static $instance = null;

    private function __construct(){
        $this->counter = 0;
    }
    
    
    // increment de 1
    public function increment(){
        $this->counter++;
    }

    public static function getInstance(){
        if(is_null(self::$instance)){
            self::$instance = new Singleton_class();
        }
        return self::$instance;
    }

    public function getCounter(){
        return $this->counter;
    }
}
