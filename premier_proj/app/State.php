<?php

namespace App;

use App\State0;
use App\State1;

class State
{
    private $etat = 0;
    private $state;

    public function __construct()
    {
        
    }

    public function operation($val)
    {
        if ($this->etat == 0)
        {
            $state = new State0();
            $this->etat = 1;
            return $state->operation($val);
        }
        else
        {
            $state = new State1();
            $this->etat = 0;
            return $state->operation($val);
        }
    }
}
