<?php 

namespace App;

use App\VoitureTemplate;

class FiatVoiture extends VoitureTemplate{

    public function getOptions(){
        return parent::getOptions() . ' - Climatisation de fiat';
    }
}