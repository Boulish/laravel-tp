<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Concession
{
    private $voiture = null;

    public function __constrcut(){

    }

    public function fetch_car($voiture){
        $this->voiture = $voiture;
    }


    public function get_car() {
        return $this->voiture;
    }

    public function toString(){
        return 'Concession automobile';
    }
}
