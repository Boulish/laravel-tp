<?php

namespace App;

class Observer
{
    private $counter;

    public function __construct(){
        $this->counter=0;
    }

    public function update(){
        $this->counter++;
    }


    public function display(){
        if($this->counter === 0){
            return "J'ai été notifié aucune fois.";
        }else{
            return "J'ai été notifié " . $this->counter . "fois.";
        }
    }
}


