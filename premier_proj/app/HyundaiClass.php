<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HyundaiClass
{
    private $immatriculation;
    private $marque = 'hyundai';

    public function __construct($immatriculation){
        $this->immatriculation = $immatriculation;
    }

    public function toString(){
        return $this->immatriculation . $this->marque;
    }

    public function getMarque(){
        return $this->marque;
    }
}
