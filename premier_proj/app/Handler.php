<?php

namespace App;

use App\HandlerSon1;
use App\HandlerSon2;
use App\HandlerSon3;

class Handler
{
    public function __CONSTRUCT()
    {

    }

    public function handle($val)
    {
        switch(gettype($val))
        {
            case "integer":
                $handler = new HandlerSon1();
                return $handler->handle();
                break;
            case "string":
                $handler = new HandlerSon2();
                return $handler->handle();
                break;
            default:
                $handler = new HandlerSon3();
                return $handler->handle();
                break;
        }
    }
}