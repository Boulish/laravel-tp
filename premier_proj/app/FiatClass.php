<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FiatClass
{
    private $immatriculation;
    private $marque = 'fiat';

    public function __construct($immatriculation){
        $this->immatriculation = $immatriculation;
    }

    public function toString(){
        return $this->immatriculation . $this->marque;
    }

    public function setImmatriculation($immat){
        $this->immatriculation= $immat;
    }

    public function getMarque(){
        return $this->marque;
    }
}
