<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Singleton_class;
use App\FacadeCommande;
use App\ArrayVoiture;
use App\Subject;
use App\Observer;
use App\FactoryClass;
use App\Strategy;
use App\FiatVoiture;
use App\HyundaiVoiture;
use App\OtherVoiture;
use App\Noeud;
use App\Handler;
use App\State;

class Firstcontroller extends Controller
{
    
    public function home(){
        return view('home');
    }

    public function singleton(){
        // methode inutile :
        $a = Singleton_class::getInstance();
        $b = Singleton_class::getInstance();

        return view('affichage_singleton', [
            'instance_a' => $a,
            'instance_b' => $b
        ]);
    }

    public function testFacade(){
        $result = FacadeCommande::commander("fiat", "Super fiat punto");
        $result2 = FacadeCommande::commander("hyundai", "Hyundai satelite");

        return view('affichage_facadeCommande', [
            'resultat' => $result,
            "resultat2" => $result2
        ]);
    }


    public function testIterator(){
        
        
        // creation de 10 voitures, pour iterer dessus dans ma vue....
        for($i=0; $i<10; $i++){
            $datas[$i] = FactoryClass::create_car('fiat', "imat".$i);
        }
        
        $mes_voitures = new ArrayVoiture($datas);

        return view('iterateur', [
            'mes_voitures' => $mes_voitures->initIterator()
        ]);
    }



    public function testObserver(){

        $sub = new Subject();
        $obs1 = new Observer();
        $obs2 = new Observer();
        $obs3 = new Observer();

        $sub->addObserver($obs1);
        $sub->addObserver($obs2);
        $sub->addObserver($obs3);


        return view('affichage_observer', [
            'sub' => $sub,
            'obs1' =>$obs1,
            'obs2' =>$obs2
        ]);
    }


    public function testStrategy(){

        $voitureFrancaise = new Strategy('FR');
        $voitureAllemande = new Strategy('ALL');

        return view('affichage_strategy', [
            'v1' => $voitureFrancaise,
            'v2' => $voitureAllemande
        ]);
    }


    public function testTemplate(){
        $fiat = new FiatVoiture();
        $hyundai = new HyundaiVoiture();
        $other = new OtherVoiture();

        return view('affichage_template', [
            'fiat'=> $fiat,
            'hyundai'=> $hyundai,
            'other'=> $other
        ]);
    }

    public function testCommand(){
        return view('affichage_command', []);
    }

    public function testComposite(){
        // je suis client : 

        // je veux un arbre :
        $feuille = new Noeud(5);
        $feuille1 = new Noeud(5);
        $feuille2 = new Noeud(5);
        $feuille3 = new Noeud(5);
        $sous_arbre = new Noeud();
        $sous_arbre->addFils($feuille1);
        $sous_arbre->addFils($feuille2);
        $sous_arbre->addFils($feuille3);
        $arbre = new Noeud();
        $arbre->addFils($sous_arbre);
        $arbre->addFils($feuille);

        return view('affichage_composite', [
            'prix_arbre' => $arbre->getPrixTotal(),
            'prix_sous_arbre' => $sous_arbre->getPrixTotal()
        ]);
    }

    public function testResponsabilityChain()
    {
        $string = "une chaine de caractères";
        $integer = 5;
        $float = 9.99;

        $handler = new Handler();

        return view('affichage_responsability', [
            'string'=>$handler->handle($string),
            'integer'=>$handler->handle($integer),
            'float'=>$handler->handle($float)
        ]);
    }

    public function testState(){
        
        $valeur = 0;
        $state = new State();

        $etat0 = $state->operation($valeur);
        $etat1 = $state->operation($valeur);

        return view('affichage_state', ['etat0'=>$etat0, 'etat1'=>$etat1]);
    }

}
