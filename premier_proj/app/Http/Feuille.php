<?php

class Feuille implements Composite
{
    private $prix=0;

    public function __construct($initPrix){
        $this->prix = $initPrix;
    }


    public function getPrixTotal(){
        return $this->prix;
    }
}


?>