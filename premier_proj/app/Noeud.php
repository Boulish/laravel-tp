<?php

namespace App;

use App\Composite;

class Noeud implements Composite 
{
    private $tab_fils = [];
    private $prix;
    public function __construct($prixInit = 0){
        $this->prix=$prixInit; 
    }

    public function getPrixTotal(){
        $res = 0;
        if(count($this->tab_fils) > 0){
            foreach($this->tab_fils as $elem){
                $res += $elem->getPrixTotal();
            }
        }else{
            $res = $this->prix;
        }

        return $res;
    }

    public function addFils($fils){
        $this->tab_fils[] = $fils;
    }
}


