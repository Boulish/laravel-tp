<?php

namespace App;

class Iterator
{

    private $array_data;
    private $index = 0;
    private $size;

    public function __construct($datas){
        $this->array_data = $datas;
        $this->size = count($datas);
        $this->index = $this->size;
    }

    public function next(){
        $res = $this->array_data[ ( $this->size - $this->index ) ];
        $this->index--;
        return $res;
    }

    public function hasNext(){
        return ($this->index !== 0);
    }

}
