<?php

namespace App;

use App\FactoryClass;
use App\Concession;

class FacadeCommande 
{
    public function __construct(){

    }

    public static function commander($type, $nom){
        $result = [];

        $voiture = FactoryClass::create_car($type,$nom);
        $result['factory'] = new FactoryClass;

        $concession = new Concession();
        $concession->fetch_car($voiture);
        $result['concession'] = $concession;

        $facturation = new Facturation();
        $facturation->make_facture($voiture);
        $result['facturation'] = $facturation;

        return $result;
    }

}
