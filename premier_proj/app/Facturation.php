<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facturation 
{
    private $prix = null;
    public function __construct(){

    }

    public function make_facture($voiture){
        if ($voiture->getMarque() === 'hyundai'){
            $this->prix = 12000;
        }
        else if($voiture->getMarque() === 'fiat'){
            $this->prix = 15000;
        }
    }

    public function getPrix(){
        return $this->prix;
    }

    public function toString(){
        return 'Facturation de ' . $this->getPrix();
    }
}
