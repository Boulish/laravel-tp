<?php

namespace App;

use App\StrategyAllemagne;
use App\StrategyFrance;

class Strategy
{
    private $strat;

    public function __construct($pays){
        if ($pays === 'FR'){
            $this->strat = new StrategyFrance();
        }

        if($pays === 'ALL'){
            $this->strat = new StrategyAllemagne();
        }
    }

    public function getTVA(){
        return $this->strat->getTVA();
    }

}