<?php 

namespace App;


interface Composite {

    public function getPrixTotal();
}