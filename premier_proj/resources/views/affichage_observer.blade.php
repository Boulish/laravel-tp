@extends('base')

@section('content')
    <p>Obs 1: {{$obs1->display()}}</p>
    <p>Obs 2: {{$obs2->display()}}</p>

    <p>On change le sujet (2 fois):</p>
    {{$sub->notifyObservers()}}
    {{$sub->notifyObservers()}}

    <p>Obs 1 :{{$obs1->display()}}</p>
    <p>Obs 2 :{{$obs2->display()}}</p>   
@endsection                