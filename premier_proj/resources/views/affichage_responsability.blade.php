@extends('base');

:@section('content')

    <p>La variable String retourne : {{$string}}</p>

    <p>La variable integer retourne : {{$integer}}</p>

    <p>La variable float retourne : {{$float}}</p>

    <p>On voit bien le message d'erreur car le type n'a pas été reconnu</p>

@endsection