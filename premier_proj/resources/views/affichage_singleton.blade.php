@extends('base')

@section('content')
    <div class="m-b-m">Le compteur de l'instance a est : {{ $instance_a->getCounter() }}, celui de b : {{ $instance_b->getCounter() }}</div>
    <div class=""> {{ $instance_a->increment() }}- Increment du singleton sur a - </div> 
    <div class="">Le compteur de l'instance b est désormais : {{ $instance_b->getCounter() }}</div>
    <div class="">Le compteur de l'instance a est désormais : {{ $instance_a->getCounter() }}</div>
    <div class="">On montre que les compteurs sont les mêmes. ( le compteur de b a été incrémenté alors que l'on a pas appelé increment() sur la variable b</div>
@endsection