@extends('base');

:@section('content')

    <h1>Commande :</h1>
    
    <h3>
        
        Le patron de conception commande, c'est par exemple, le système de routage dans laravel ou d'autres framework.
        Le router ne connait qu'une liste de route, et fait ainsi appel aux controleurs,
        <br/> qui eux, vont contenir le code à exécuter correspondant à telle ou telle route.

    </h3>
@endsection